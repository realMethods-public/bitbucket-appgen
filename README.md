# realMethods Project Generator For Bitbucket

![alt text](http://www.realmethods.com/img/bitbucket-realmethods.png)

The contained Bitbucket configuration file (_bitbucket-pipelines.yml_) is a simple, yet powerful way to leverage realMethods and Bitbucket to automate the generation of an MVP-quality application built on, tested, and deployed through your Bitbucket CI/CD pipeline.

This repository contains a configuration YAML file along with [sample files](https://bitbucket.org/realMethods-public/bitbucket-appgen/src/master/samples/) as an example of input to the application generation process.

To take a quick test drive, follow the instructions in the *Quick Start* section.  Skip to the _Step-By-Step_ section to generate an application using more customized inputs.

## Quick Start

In this section you will make the least amount of changes to see application generation in action.  

1. Clone this project in one of two ways:

`SSH - git clone git@bitbucket.org:realMethods-public/bitbucket-appgen.git`

`HTTPS - git clone https://realMethods-public@bitbucket.org/realMethods-public/bitbucket-appgen.git`

2. Create a Bitbucket repository by the name of _Django_.
3. Edit the `/samples/git/bitbucket-repo-params.yml` file to use your Bitbucket user id and password.
4. Commit your changes to this project to a new Bitbucket repo and observe its CI/CD pipeline status.
5. Once the job in the pipeline is complete, observe the pipeline status of the _Django_ repo. An application will have been generated using a default model and a Django stack.
6. The application will eventually be built and tested:


# Step-By-Step Application Generation
## Step 1 - Clone This Project
Clone this project in one of two ways:

`SSH - git clone git@bitbucket.org:realMethods-public/bitbucket-appgen.git`

`HTTPS - git clone https://realMethods-public@bitbucket.org/realMethods-public/bitbucket-appgen.git`

## Step 2 - Make Changes
#### API_TOKEN
An api_token is required to initialize a unique session with the realMethods back-end.  The one provided as default is safe to 
use as a Bitbucket user.

`export API_TOKEN=NXSaoHyVf7uDeqhT`

#### Application Generation Input Files 
To invoke application generation, 4 input files are required.  These files must be found in or relative to the root of a Bitbucket repository. These files will referenced in the cloned _bitbucket-pipelines.yml_ file. Take some time to browse the _samples_ directory of the locally cloned repository.

Although the contents of each type of configuration file is fairly straightforward, [click here](https://realmethods.com/home/command-line-interface/) to learn more.

#### GENERATE\_YAML\_FILE:
This YAML file contains the directives required to generate an application using a model identifier (by id or file_path), technology stack (by id or name), application options JSON file, and the Bitbucket repository params file (where to commit the generated application files to). For the purpose of this exercise, the model file, application options file and Bitbucket repository file will be passed in as command line arguments instead of read in from this file.

Learn more [here](https://realmethods.com/home/command-line-interface/). 

See an example [here](https://bitbucket.org/realMethods-public/bitbucket-appgen/src/master/samples/yamls/generate-django.yml).

`export GENERATE_YAML_FILE=samples/yamls/generate-django.yml`
  
#### GIT\_PARAMS\_FILE:
This YAML file contains one or more groupings of parameters to control committing an application's files (language specific source code, build files, config files, Bitbucket config file, etc..) to a Bitbucket repository. If this argument is not provided, the _gitParams-->file_ param of the _generation-yaml-file_ is used.  

  
Learn more [here](https://realmethods.com/home/command-line-interface/).

See an example [here](https://bitbucket.org/realMethods-public/bitbucket-appgen/src/master/samples/git/bitbucket-repo-params.yml)

`export GIT_PARAMS_FILE=samples/git/bitbucket-repo-params.yml`

#### APP\_OPTIONS\_FILE:  
This JSON file contains one or more groupings of parameters to control application generation content output. This file is where you would provide such things as database access params, application params (name, description, etc..), and so forth. If this argument is not provided the _appOptionsFile_ param of the _generation-yaml-file_ is used.  

Learn more [here](https://realmethods.com/home/command-line-interface/).

See an example [here](https://bitbucket.org/realMethods-public/bitbucket-appgen/src/master/samples/options/Django.options.json).

`export APP_OPTIONS_FILE=samples/options/Django.options.json`

#### MODEL_FILE:  
This file contains a structured description of the entities that make up your application.  Concepts like Account, Customer, Address, and so forth.  Also includec would be the data of each entity (name, balance, etc...) along with how they relate to each other. ([See supported models](https://realmethods.com/home/models/)).  

Learn [more on models](https://realmethods.com/home/models/).

See an example [here](https://bitbucket.org/realMethods-public/bitbucket-appgen/src/master/samples/models/reference_management.xmi).

`export MODEL_FILE: "samples/models/reference_management.xmi`

##### AWS Credentials
Note: If using one of the AWS Lambda stacks, you will have to assign the access key and secret key as project level environment variables.  See [Variables in Pipelines](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html) for more details. Be sure to name the accesskey USER\_AWS\_ACCESSKEY and name the secretkey USER\_AWS\_SECRETKEY.  Equally important, 
make sure you have the correct policies assigned for the related user (_AWSCodeDeployRoleForLambda, AWSLambdaExecute, AWSLambdaRole_, etc..)

## Step 3 - Create a Project For Generated App
Create a Bitbucket project repository (Repo A) for the generated application files.  This repository name must be the name you assigned in the bitbucket-repo-params.yml sample file (the file that contains your Bitbucket parameters - see section *GIT_PARAMS_FILE* above ).

## Step 4 - Create a Project For Cloned Project
Create another Bitbucket project repository (Repo B) to commit this project to.  Any name will do.  You are committing the modified _bitbucket-pipelines.yml_ file along with the 4 input files discussed above.  Only the _bitbucket-pipelines.yml_ must be in the root.  Place the 4 input files wherever you wish but be sure to reference them correctly within the _bitbucket-pipelines.yml_ file.   

## Step 5 - Commit Your Project
Upon committing this project to Repo B, the _bitbucket-pipelines.yml_ should begin running within a Bitbucket pipeline. Your application is now being generatd by realMethods.

![alt text](http://www.realmethods.com/img/bitbucket-job-running.png)

## Step 6 - Watch Pipeline Execution
Upon completion of Step 5, realMethods will commit all generated application files to Repo A.  This should cause a Bitbucket pipeline to run the generated _bitbucket-pipelines.yml_ file.  The generated application is now being built and tested.

![alt text](http://www.realmethods.com/img/bitbucket-app-build-running-results.png)

## Congratulations!
Using the power of Bitbucket and realMethods, you just generated, built, and tested an entire application complete with core capabilities, build file, CI/CD config, and much more....

Best of luck in completing the application!

